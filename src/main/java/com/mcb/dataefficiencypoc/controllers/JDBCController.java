package com.mcb.dataefficiencypoc.controllers;

import com.mcb.dataefficiencypoc.services.JDBCService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jdbc")
@AllArgsConstructor
public class JDBCController {

    private final JDBCService jdbcService;

    @GetMapping("/single/10k")
    public ResponseEntity<?> getEmployeesBySingleDBCall() {
        return new ResponseEntity<>(jdbcService.getEmployeesBySingleDBCall(), HttpStatus.OK);
    }

    @GetMapping("/single/100k")
    public ResponseEntity<?> getEmployeesByFewSingleDBCalls() {
        return new ResponseEntity<>(jdbcService.getEmployeesByFewSingleDBCalls(), HttpStatus.OK);
    }

    @GetMapping("/iterative/10k")
    public ResponseEntity<?> getEmployeesByMultipleDBCalls() {
        return new ResponseEntity<>(jdbcService.getEmployeesByMultipleDBCalls(), HttpStatus.OK);
    }

    @GetMapping("/iterative/100k")
    public ResponseEntity<?> getEmployeesByManyMultipleDBCalls() {
        return new ResponseEntity<>(jdbcService.getEmployeesByManyMultipleDBCalls(), HttpStatus.OK);
    }

}
