package com.mcb.dataefficiencypoc.controllers;

import com.mcb.dataefficiencypoc.services.JPAService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dataJPA")
@AllArgsConstructor
public class DataJPAController {

    private final JPAService jpaService;

    @GetMapping("/single/10k")
    public ResponseEntity<?> getEmployeesBySingleDBCall() {
        return new ResponseEntity<>(jpaService.getEmployeesBySingleDBCall(), HttpStatus.OK);
    }

    @GetMapping("/single/100k")
    public ResponseEntity<?> getEmployeesByFewSingleDBCalls() {
        return new ResponseEntity<>(jpaService.getEmployeesByFewSingleDBCalls(), HttpStatus.OK);
    }

    @GetMapping("/iterative/10k")
    public ResponseEntity<?> getEmployeesByMultipleDBCalls() {
        return new ResponseEntity<>(jpaService.getEmployeesByMultipleDBCalls(), HttpStatus.OK);
    }

    @GetMapping("/iterative/100k")
    public ResponseEntity<?> getEmployeesByManyMultipleDBCalls() {
        return new ResponseEntity<>(jpaService.getEmployeesByManyMultipleDBCalls(), HttpStatus.OK);
    }

}
