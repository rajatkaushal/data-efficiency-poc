package com.mcb.dataefficiencypoc.controllers;

import com.mcb.dataefficiencypoc.services.QueryDSLService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/queryDSL")
@AllArgsConstructor
public class QueryDSLController {

    private final QueryDSLService queryDSLService;

    @GetMapping("/single/10k")
    public ResponseEntity<?> getEmployeesBySingleDBCall() {
        return new ResponseEntity<>(queryDSLService.getEmployeesBySingleDBCall(), HttpStatus.OK);
    }

    @GetMapping("/single/100k")
    public ResponseEntity<?> getEmployeesByFewSingleDBCalls() {
        return new ResponseEntity<>(queryDSLService.getEmployeesByFewSingleDBCalls(), HttpStatus.OK);
    }

    @GetMapping("/iterative/10k")
    public ResponseEntity<?> getEmployeesByMultipleDBCalls() {
        return new ResponseEntity<>(queryDSLService.getEmployeesByMultipleDBCalls(), HttpStatus.OK);
    }

    @GetMapping("/iterative/100k")
    public ResponseEntity<?> getEmployeesByManyMultipleDBCalls() {
        return new ResponseEntity<>(queryDSLService.getEmployeesByManyMultipleDBCalls(), HttpStatus.OK);
    }

}
