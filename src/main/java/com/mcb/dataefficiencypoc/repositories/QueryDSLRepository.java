package com.mcb.dataefficiencypoc.repositories;

import com.mcb.dataefficiencypoc.*;
import com.mcb.dataefficiencypoc.models.*;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.QBean;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLQueryFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.querydsl.core.types.Projections.bean;

@Repository
@AllArgsConstructor
public class QueryDSLRepository {

    private SQLQueryFactory sqlQueryFactory;

    private static final QEmployee emp = new QEmployee("emp");
    private static final QAssignment ass = new QAssignment("ass");
    private static final QDepartment dep = new QDepartment("dep");
    private static final QDesignation des = new QDesignation("des");
    private static final QDepartment a_dep = new QDepartment("a_dep");
    private static final QDesignation a_des = new QDesignation("a_des");
    private static final QServiceHistory sh = new QServiceHistory("sh");
    private static final QEducationalQualification eq = new QEducationalQualification("eq");

    private static final QBean<Employee> employeeBean = bean(Employee.class, emp.id, emp.code, emp.name, emp.dob, emp.gender,
            emp.dateofjoining, emp.dateofleaving, emp.employeestatus,
            bean(Department.class, dep.all()).as("department"),
            bean(Designation.class, des.all()).as("designation"),
            bean(Assignment.class, ass.id, ass.fromdate, ass.todate, ass.isprimary,
                    bean(Department.class, a_dep.all()).as("department"),
                    bean(Designation.class, a_des.all()).as("designation")
            ).as("assignment"),
            bean(ServiceHistory.class, sh.all()).as("serviceHistory"),
            bean(EducationalQualification.class, eq.all()).as("educationalQualification")
    );

    private SQLQuery<?> getDSLQuery() {
/*
        return sqlQueryFactory.from(emp)
                .join(dep).on(emp.departmentid.eq(dep.id))
                .join(des).on(emp.designationid.eq(des.id))
                .join(sh).on(emp.id.eq(sh.employeeid))
                .join(eq).on(emp.id.eq(eq.employeeid))
                .join(ass).on(emp.id.eq(ass.employeeid))
                .join(a_dep).on(a_dep.id.eq(ass.departmentid))
                .join(a_des).on(a_des.id.eq(ass.designationid))
                .where(emp.employeestatus.in("EMPLOYED", "TERMINATED").and(ass.isprimary.eq(true)));
*/
        return sqlQueryFactory.select(
                emp.id.as("e_id"), emp.code.as("e_code"), emp.name.as("e_name"), emp.dob.as("e_dob"),
                emp.gender.as("e_gender"), emp.dateofjoining.as("e_dateOfJoining"), emp.dateofleaving.as("e_dateOfLeaving"),
                emp.employeestatus.as("e_employeeStatus"), emp.physicallydisabled.as("e_physicallyDisabled"),
                emp.maritalstatus.as("e_maritalStatus"), emp.address.as("e_address"),
                dep.id.as("dep_id"), dep.name.as("dep_name"), dep.code.as("dep_code"), dep.active.as("dep_active"),
                des.id.as("des_id"), des.name.as("des_name"), des.code.as("des_code"), des.active.as("des_active"),
                ass.id.as("a_id"), ass.isprimary.as("a_isPrimary"), ass.fromdate.as("a_fromDate"), ass.todate.as("a_toDate"),
                a_dep.id.as("a_dep_id"), a_dep.name.as("a_dep_name"), a_dep.code.as("a_dep_code"), a_dep.active.as("a_dep_active"),
                a_des.id.as("a_des_id"), a_des.name.as("a_des_name"), a_des.code.as("a_des_code"), a_des.active.as("a_des_active"),
                sh.id.as("s_id"), sh.company.as("s_company"), sh.servicefrom.as("s_serviceFrom"), sh.servicetill.as("s_serviceTill"),
                eq.id.as("eq_id"), eq.qualification.as("eq_qualification"), eq.majorsubject.as("eq_majorSubject"),
                eq.yearofpassing.as("eq_yearOfPassing"), eq.university.as("eq_university")
        ).from(emp)
                .join(dep).on(emp.departmentid.eq(dep.id))
                .join(des).on(emp.designationid.eq(des.id))
                .join(sh).on(emp.id.eq(sh.employeeid))
                .join(eq).on(emp.id.eq(eq.employeeid))
                .join(ass).on(emp.id.eq(ass.employeeid))
                .join(a_dep).on(a_dep.id.eq(ass.departmentid))
                .join(a_des).on(a_des.id.eq(ass.designationid))
                .where(emp.employeestatus.in("EMPLOYED", "TERMINATED").and(ass.isprimary.eq(true)));
    }

    public List<Employee> getEmployees() {
        return getDSLQuery().transform(GroupBy.groupBy(emp.id).list(employeeBean));
    }

    public List<Employee> getPaginatedEmployees(int pageNumber, int pageSize) {
        return getDSLQuery()
                .limit(pageSize)
                .offset(pageNumber * pageSize)
                .transform(GroupBy.groupBy(emp.id).list(employeeBean));
    }
}


/*
    SELECT emp.id, emp.id, emp.code, emp.name, emp.dob, emp.gender, emp.dateofjoining, emp.dateofleaving, emp.employeestatus,
            dep.active, dep.code, dep.id, dep.name,
            des.active, des.code, des.id, des.name,
            ass.id, ass.fromdate, ass.todate, ass.isprimary,
                a_dep.active, a_dep.code, a_dep.id, a_dep.name,
                a_des.active, a_des.code, a_des.id, a_des.name,
            sh.company, sh.employeeid, sh.id, sh.servicefrom, sh.servicetill,
            eq.employeeid, eq.id, eq.majorsubject, eq.qualification, eq.university, eq.yearofpassing
    FROM employee emp join department dep on emp.departmentid = dep.id
            JOIN designation des ON emp.designationid = des.id
            JOIN service_history sh ON emp.id = sh.employeeid
            JOIN educational_qualification eq ON emp.id = eq.employeeid
            JOIN assignment ass ON emp.id = ass.employeeid
                JOIN department a_dep ON a_dep.id = ass.departmentid
                JOIN designation a_des ON a_des.id = ass.designationid
    WHERE emp.employeestatus IN (?, ?) AND ass.isprimary = ?
    LIMIT ? OFFSET ?;
*/
