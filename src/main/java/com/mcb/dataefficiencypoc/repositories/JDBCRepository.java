package com.mcb.dataefficiencypoc.repositories;

import com.mcb.dataefficiencypoc.models.Employee;
import com.mcb.dataefficiencypoc.repositories.helpers.EmployeeRowMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
@AllArgsConstructor
public class JDBCRepository {

    private final EmployeeRowMapper employeeRowMapper;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @SuppressWarnings("SqlResolve")
    private static final String QUERY = "SELECT e.id AS e_id, e.code AS e_code, e.name AS e_name, e.dob AS e_dob," +
            " e.gender AS e_gender, e.dateOfJoining AS e_dateOfJoining, e.dateOfLeaving AS e_dateOfLeaving," +
            " e.employeeStatus AS e_employeeStatus, e.physicallyDisabled AS e_physicallyDisabled," +
            " e.maritalStatus AS e_maritalStatus, e.address AS e_address," +
            " dpt.id AS dpt_id, dpt.name AS dpt_name, dpt.code AS dpt_code, dpt.active AS dpt_active," +
            " des.id AS des_id, des.name AS des_name, des.code AS des_code, des.active AS des_active," +
            " a.id AS a_id, a.isPrimary AS a_isPrimary, a.fromDate AS a_fromDate, a.toDate AS a_toDate," +
            " dpt_a.id AS dpt_a_id, dpt_a.name AS dpt_a_name, dpt_a.code AS dpt_a_code, dpt_a.active AS dpt_a_active," +
            " des_a.id AS des_a_id, des_a.name AS des_a_name, des_a.code AS des_a_code, des_a.active AS des_a_active," +
            " s.id AS s_id, s.company AS s_company, s.serviceFrom AS s_serviceFrom, s.serviceTill AS s_serviceTill," +
            " eq.id AS eq_id, eq.qualification AS eq_qualification, eq.majorSubject AS eq_majorSubject," +
            " eq.yearOfPassing AS eq_yearOfPassing, eq.university AS eq_university" +
            " FROM employee e JOIN department dpt on e.departmentId = dpt.id" +
            " JOIN designation des on e.designationId = des.id" +
            " JOIN assignment a on e.id = a.employeeId" +
            " JOIN department dpt_a on a.departmentId = dpt_a.id" +
            " JOIN designation des_a on a.designationId = des_a.id" +
            " JOIN service_history s on s.employeeId = e.id" +
            " JOIN educational_qualification eq on eq.employeeId = e.id" +
            " WHERE e.employeeStatus IN ('EMPLOYED', 'TERMINATED') AND a.isPrimary = true";

    public List<Employee> getEmployees() {
//        log.info(QUERY);
        return jdbcTemplate.query(QUERY, employeeRowMapper);
    }

    public List<Employee> getPaginatedEmployees(int pageNumber, int pageSize) {
        Map<String, Integer> namedParams = new HashMap<String, Integer>(){{
            put("pageNumber", pageNumber * pageSize);
            put("pageSize", pageSize);
        }};

//        log.info(QUERY + " LIMIT :pageSize OFFSET :pageNumber");
        return namedParameterJdbcTemplate.query(QUERY + " LIMIT :pageSize OFFSET :pageNumber", namedParams, employeeRowMapper);
    }
}
