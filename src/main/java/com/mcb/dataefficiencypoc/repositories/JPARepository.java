package com.mcb.dataefficiencypoc.repositories;

import com.mcb.dataefficiencypoc.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPARepository extends JpaRepository<Employee, Long> {
}
