package com.mcb.dataefficiencypoc.repositories.helpers;

import com.mcb.dataefficiencypoc.models.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeRowMapper implements ResultSetExtractor<List<Employee>> {
    @Override
    public List<Employee> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Employee> list = new ArrayList<>();

        while(rs.next()){
            Employee emp = Employee.builder()
                    .id(rs.getLong("e_id"))
                    .code(rs.getString("e_code"))
                    .name(rs.getString("e_name"))
                    .dob(rs.getString("e_dob"))
                    .gender(rs.getString("e_gender"))
                    .dateOfJoining(rs.getString("e_dateOfJoining"))
                    .dateOfLeaving(rs.getString("e_dateOfLeaving"))
                    .department(
                            Department.builder()
                                    .id(rs.getLong("dpt_id"))
                                    .code(rs.getString("dpt_code"))
                                    .name(rs.getString("dpt_name"))
                                    .active(rs.getBoolean("dpt_active"))
                                    .build()
                    )
                    .designation(
                            Designation.builder()
                                    .id(rs.getLong("des_id"))
                                    .code(rs.getString("des_code"))
                                    .name(rs.getString("des_name"))
                                    .active(rs.getBoolean("des_active"))
                                    .build()
                    )
                    .employeeStatus(rs.getString("e_employeeStatus"))
                    .physicallyDisabled(rs.getBoolean("e_physicallyDisabled"))
                    .maritalStatus(rs.getString("e_maritalStatus"))
                    .address(rs.getString("e_address"))
                    .assignment(
                            Assignment.builder()
                                    .id(rs.getLong("a_id"))
                                    .department(
                                            Department.builder()
                                                    .id(rs.getLong("dpt_a_id"))
                                                    .code(rs.getString("dpt_a_code"))
                                                    .name(rs.getString("dpt_a_name"))
                                                    .active(rs.getBoolean("dpt_a_active"))
                                                    .build()
                                    )
                                    .designation(
                                            Designation.builder()
                                                    .id(rs.getLong("des_a_id"))
                                                    .code(rs.getString("des_a_code"))
                                                    .name(rs.getString("des_a_name"))
                                                    .active(rs.getBoolean("des_a_active"))
                                                    .build()
                                    )
                                    .isPrimary(rs.getBoolean("a_isPrimary"))
                                    .fromDate(rs.getString("a_fromDate"))
                                    .toDate(rs.getString("a_toDate"))
                                    .build()
                    )
                    .serviceHistory(
                            ServiceHistory.builder()
                                    .id(rs.getLong("s_id"))
                                    .company(rs.getString("s_company"))
                                    .serviceFrom(rs.getString("s_serviceFrom"))
                                    .serviceTill(rs.getString("s_serviceTill"))
                                    .build()
                    )
                    .educationalQualification(
                            EducationalQualification.builder()
                                    .id(rs.getLong("eq_id"))
                                    .qualification(rs.getString("eq_qualification"))
                                    .majorSubject(rs.getString("eq_majorSubject"))
                                    .yearOfPassing(rs.getInt("eq_yearOfPassing"))
                                    .university(rs.getString("eq_university"))
                                    .build()
                    )
                    .build();
            list.add(emp);
        }

        return list;
    }
}
