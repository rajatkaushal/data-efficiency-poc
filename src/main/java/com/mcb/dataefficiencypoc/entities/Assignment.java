package com.mcb.dataefficiencypoc.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Assignment {
    @Id
    private int id;
    @OneToOne
    @JoinColumn(name = "departmentId")
    private Department department;
    @OneToOne
    @JoinColumn(name = "designationId")
    private Designation designation;
    private boolean isPrimary;
    private String fromDate;
    private String toDate;
}
