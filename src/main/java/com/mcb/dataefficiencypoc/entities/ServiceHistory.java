package com.mcb.dataefficiencypoc.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "service_history")
public class ServiceHistory {
    @Id
    private int id;
    private String company;
    private String serviceFrom;
    private String serviceTill;
}
