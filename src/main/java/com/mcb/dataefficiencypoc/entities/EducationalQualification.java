package com.mcb.dataefficiencypoc.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "educational_qualification")
public class EducationalQualification {
    @Id
    private int id;
    private String qualification;
    private String majorSubject;
    private int yearOfPassing;
    private String university;
}
