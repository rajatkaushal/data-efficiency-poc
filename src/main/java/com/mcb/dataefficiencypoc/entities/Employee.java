package com.mcb.dataefficiencypoc.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Employee {
    @Id
    private int id;
    private String code;
    private String name;
    private String dob;
    private String gender;
    private String dateOfJoining;
    private String dateOfLeaving;
    @OneToOne
    @JoinColumn(name = "departmentId")
    private Department department;
    @OneToOne
    @JoinColumn(name = "designationId")
    private Designation designation;
    private String employeeStatus;
    private boolean physicallyDisabled;
    private String maritalStatus;
    private String address;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "employeeId")
    private Assignment assignment;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "employeeId")
    private ServiceHistory serviceHistory;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "employeeId")
    private EducationalQualification educationalQualification;
}
