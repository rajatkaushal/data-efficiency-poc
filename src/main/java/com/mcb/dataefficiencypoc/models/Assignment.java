package com.mcb.dataefficiencypoc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Assignment {
    private Long id;
    private Department department;
    private Designation designation;
    private boolean isPrimary;
    private String fromDate;
    private String toDate;
}
