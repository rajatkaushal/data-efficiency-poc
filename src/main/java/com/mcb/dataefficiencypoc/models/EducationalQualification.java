package com.mcb.dataefficiencypoc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EducationalQualification {
    private Long id;
    private String qualification;
    private String majorSubject;
    private int yearOfPassing;
    private String university;
}
