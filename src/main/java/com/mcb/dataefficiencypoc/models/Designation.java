package com.mcb.dataefficiencypoc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Designation {
    private Long id;
    private String code;
    private String name;
    private boolean active;
}
