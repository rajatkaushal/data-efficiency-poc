package com.mcb.dataefficiencypoc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Long id;
    private String code;
    private String name;
    private String dob;
    private String gender;
    private String dateOfJoining;
    private String dateOfLeaving;
    private Department department;
    private Designation designation;
    private String employeeStatus;
    private boolean physicallyDisabled;
    private String maritalStatus;
    private String address;
    private Assignment assignment;
    private ServiceHistory serviceHistory;
    private EducationalQualification educationalQualification;
}
