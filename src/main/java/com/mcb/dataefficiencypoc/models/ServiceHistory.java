package com.mcb.dataefficiencypoc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceHistory {
    private Long id;
    private String company;
    private String serviceFrom;
    private String serviceTill;
}
