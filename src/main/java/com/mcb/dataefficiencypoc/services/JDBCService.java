package com.mcb.dataefficiencypoc.services;

import com.mcb.dataefficiencypoc.models.Employee;
import com.mcb.dataefficiencypoc.repositories.JDBCRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class JDBCService {

    private JDBCRepository jdbcRepository;

    public List<Employee> getEmployeesBySingleDBCall() {
        List<Employee> employees;

        long startTime = System.currentTimeMillis();
        employees = jdbcRepository.getEmployees();
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| SINGLE JDBC TEMPLATE CALL ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByFewSingleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            employees.addAll(jdbcRepository.getEmployees());
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| FEW SINGLE JDBC TEMPLATE CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByMultipleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int pageIndex = 0; pageIndex < 100; pageIndex++) {
            employees.addAll(jdbcRepository.getPaginatedEmployees(pageIndex, 100));
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| MULTIPLE JDBC TEMPLATE CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByManyMultipleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            for (int pageIndex = 0; pageIndex < 100; pageIndex++) {
                employees.addAll(jdbcRepository.getPaginatedEmployees(pageIndex, 100));
            }
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| MANY MULTIPLE JDBC TEMPLATE CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }
}
