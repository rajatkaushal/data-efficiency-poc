package com.mcb.dataefficiencypoc.services;

import com.mcb.dataefficiencypoc.models.Employee;
import com.mcb.dataefficiencypoc.repositories.QueryDSLRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class QueryDSLService {

    private QueryDSLRepository queryDSLRepository;

    public List<Employee> getEmployeesBySingleDBCall() {
        List<Employee> employees;

        long startTime = System.currentTimeMillis();
        employees = queryDSLRepository.getEmployees();
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| SINGLE QUERY DSL CALL ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByFewSingleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            employees.addAll(queryDSLRepository.getEmployees());
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| FEW SINGLE QUERY DSL CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByMultipleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int pageIndex = 0; pageIndex < 100; pageIndex++) {
            employees.addAll(queryDSLRepository.getPaginatedEmployees(pageIndex, 100));
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| MULTIPLE QUERY DSL CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByManyMultipleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            for (int pageIndex = 0; pageIndex < 100; pageIndex++) {
                employees.addAll(queryDSLRepository.getPaginatedEmployees(pageIndex, 100));
            }
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| MANY MULTIPLE QUERY DSL CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }
}
