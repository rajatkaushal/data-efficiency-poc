package com.mcb.dataefficiencypoc.services;

import com.mcb.dataefficiencypoc.entities.Employee;
import com.mcb.dataefficiencypoc.repositories.JPARepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class JPAService {

    private JPARepository jpaRepository;

    public List<Employee> getEmployeesBySingleDBCall() {
        List<Employee> employees;

        long startTime = System.currentTimeMillis();
        employees = jpaRepository.findAll(Pageable.unpaged()).getContent();
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| SINGLE DATA JPA CALL ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByFewSingleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            employees.addAll(jpaRepository.findAll(Pageable.unpaged()).getContent());
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| FEW SINGLE DATA JPA CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByMultipleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int pageIndex = 0; pageIndex < 100; pageIndex++) {
            employees.addAll(jpaRepository.findAll(PageRequest.of(pageIndex, 100)).getContent());
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| MULTIPLE DATA JPA CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }

    public List<Employee> getEmployeesByManyMultipleDBCalls() {
        List<Employee> employees = new ArrayList<>();

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            for (int pageIndex = 0; pageIndex < 100; pageIndex++) {
                employees.addAll(jpaRepository.findAll(PageRequest.of(pageIndex, 100)).getContent());
            }
        }
        long endTime = System.currentTimeMillis();

        log.info("~~~~~~~~~|| MANY MULTIPLE DATA JPA CALLS ||~~~~~~~~~");
        log.info("Start time :: " + startTime);
        log.info("End time :: " + endTime);
        log.info("Total time :: " + (endTime - startTime));

        return employees;
    }
}
