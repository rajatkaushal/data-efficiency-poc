CREATE TABLE employee
(
    id                 BIGINT       NOT NULL,
    code               VARCHAR(10),
    name               VARCHAR(50),
    dob                VARCHAR(10),
    gender             VARCHAR(1),
    dateOfJoining      VARCHAR(10),
    dateOfLeaving      VARCHAR(10),
    departmentId       BIGINT,
    designationId      BIGINT,
    employeeStatus     VARCHAR(25),
    physicallyDisabled BOOLEAN      NOT NULL,
    maritalStatus      VARCHAR(25),
    address            VARCHAR(100) NOT NULL,

    CONSTRAINT pk_employee PRIMARY KEY (Id),
    CONSTRAINT fk_employee_departmentId FOREIGN KEY (departmentId) REFERENCES department (id),
    CONSTRAINT fk_employee_designationId FOREIGN KEY (designationId) REFERENCES designation (id)
);
