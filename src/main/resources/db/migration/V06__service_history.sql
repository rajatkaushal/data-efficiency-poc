CREATE TABLE service_history
(
    id          BIGINT       NOT NULL,
    employeeId  BIGINT       NOT NULL,
    company     VARCHAR(250) NOT NULL,
    serviceFrom VARCHAR(10)  NOT NULL,
    serviceTill VARCHAR(10)  NOT NULL,

    CONSTRAINT pk_service_history PRIMARY KEY (Id),
    CONSTRAINT fk_service_history_employeeId FOREIGN KEY (employeeId) REFERENCES employee (id)
);
