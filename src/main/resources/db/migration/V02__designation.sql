CREATE TABLE designation
(
    id          BIGINT       NOT NULL,
    name        VARCHAR(100) NOT NULL,
    code        VARCHAR(3)  NOT NULL,
    active      BOOLEAN      NOT NULL,

    CONSTRAINT pk_designation PRIMARY KEY (id),
    CONSTRAINT uk_designation_code UNIQUE (code)
);
