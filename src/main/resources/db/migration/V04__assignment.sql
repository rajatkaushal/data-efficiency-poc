CREATE TABLE assignment
(
    id            BIGINT      NOT NULL,
    employeeId    BIGINT,
    departmentId  BIGINT      NOT NULL,
    designationId BIGINT      NOT NULL,
    isPrimary     BOOLEAN     NOT NULL,
    fromDate      VARCHAR(10) NOT NULL,
    toDate        VARCHAR(10) NOT NULL,

    CONSTRAINT pk_assignment PRIMARY KEY (id),
    CONSTRAINT fk_assignment_employeeId FOREIGN KEY (employeeId) REFERENCES employee (id),
    CONSTRAINT fk_assignment_departmentId FOREIGN KEY (departmentId) REFERENCES department (id),
    CONSTRAINT fk_assignment_designationId FOREIGN KEY (designationId) REFERENCES designation (id)
);
