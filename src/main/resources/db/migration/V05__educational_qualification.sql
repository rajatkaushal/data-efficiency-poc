CREATE TABLE educational_qualification
(
    id            BIGINT       NOT NULL,
    employeeId    BIGINT       NOT NULL,
    qualification VARCHAR(250) NOT NULL,
    majorSubject  VARCHAR(250),
    yearOfPassing INT          NOT NULL,
    university    VARCHAR(250),

    CONSTRAINT pk_educational_qualification PRIMARY KEY (Id),
    CONSTRAINT fk_educational_qualification_employeeId FOREIGN KEY (employeeId) REFERENCES employee (id)
);
