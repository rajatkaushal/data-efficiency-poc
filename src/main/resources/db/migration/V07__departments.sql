INSERT INTO department (id, name, code, active) VALUES
(111111111, 'Production', 'PRD', true),
(111111112, 'Research & Development', 'R&D', true),
(111111113, 'Purchasing', 'PRC', true),
(111111114, 'Marketing', 'MKT', true),
(111111115, 'Human Resource Management', 'HRM', true),
(111111116, 'Accounting & Finance', 'ACC', true);
