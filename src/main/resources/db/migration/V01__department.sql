CREATE TABLE department
(
    id     BIGINT       NOT NULL,
    name   VARCHAR(100) NOT NULL,
    code   VARCHAR(3)  NOT NULL,
    active BOOLEAN      NOT NULL,

    CONSTRAINT pk_department PRIMARY KEY (id),
    CONSTRAINT uk_department_code UNIQUE (code)
);
